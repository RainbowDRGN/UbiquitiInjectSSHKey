i=2
network=10.20.1.
username=$(cat user.txt)
pass=$(cat pass.txt)

while [ "$i" -le "255" ]; do
 ##Set timeout function for 15 seconds per IP (we are using local networks, usually only takes 5 or 6 at most)
 Timeout=15
 function timeout_mon() {
  sleep "$Timeout"
 }
 timeout_mon "$$" &
 Timeout_monitor_pid=$!
 ##Set IP address
 ipaddress=$network$i
 ##Check to see if online, sends 1 ping for 1 second.
 ping $ipaddress -w 1 -q -c 1 > /dev/null
 if ! [ $? -eq 0 ]
  then
   ##If offline, increment i, write to offline.log, kill timeout, continue
   i=$(($i+1))
   echo "$ipaddress is offline." &>> offline.log
   kill "$Timeout_monitor_pid"
   continue
  else
   #If online, copy key
   sshpass -f pass.txt ssh-copy-id -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null $username@$ipaddress 
   #If copy key fails, write to log, increment i, kill timeout, continue
   if ! [ $? -eq 0 ]
    then
     echo "IP $ipaddress did not respond normally." >> fail.log
     i=$(($i+1))
     kill "$Timeout_monitor_pid"
     continue
    #Otherwise, congratulate self.
    else
     echo "$ipaddress received key." >> win.log
    fi
   #Save new key to flash memory from config file, write to fail.log if fails
   ssh -oBatchMode=yes -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null $username@$ipaddress cfgmtd -w -p /etc || echo "$ipaddress failed to save config." >> fail.log
   i=$(($i+1))
 fi
kill "$Timeout_monitor_pid"
done